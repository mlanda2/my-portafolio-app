import { useState } from "react";

import NavBar from "./components/NavBar";
import SideBar from "./components/SideBar";

function App() {
  return (
    <div className="flex">
      <SideBar />
      <NavBar />
    </div>
  );
}

export default App;

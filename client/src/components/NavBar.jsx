import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faUser,
  faAddressCard,
} from "@fortawesome/free-solid-svg-icons";

import "./NavBar.css";
function NavBar() {
  return (
    <div className=" bg-slate-700 flex w-screen h-40 justify-center align-middle items-center">
      <h1 className=" text-white text-5xl font-sans font-semibold">
        My Portafolio
      </h1>
    </div>
  );
}

export default NavBar;

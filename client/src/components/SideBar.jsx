import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faUser,
  faBook,
  faAddressCard,
  faAngleLeft,
} from "@fortawesome/free-solid-svg-icons";

function SideBar() {
  const [sidebarVisible, setSidebarVisible] = useState(true);

  const toggleSidebar = () => {
    setSidebarVisible(!sidebarVisible);
  };

  return (
    <div
      className={`h-screen bg-gray-800 px-4 font-sans font-semibold text-white ${
        sidebarVisible ? "w-max" : "w-0"
      } transition-all ease-in-out duration-300`}
    >
      <div className="flex items-end justify-end py-2">
        <button
          className="shadow-sm rounded-lg bg-slate-700 w-8 h-8 hover:bg-slate-950"
          onClick={toggleSidebar}
        >
          <FontAwesomeIcon icon={faAngleLeft} className="" />
        </button>
      </div>
      <ul className="flex justify-around flex-col py-2 w-max">
        <li className="py-2 px-2 hover:bg-slate-600 hover:rounded-lg">
          <FontAwesomeIcon icon={faHome} />
          <a className="px-2">Home</a>
        </li>
        <li className="py-2 px-2 hover:bg-slate-600 hover:rounded-lg">
          <FontAwesomeIcon icon={faUser} />
          <a className="px-2">About</a>
        </li>
        <li className="py-2 px-2 hover:bg-slate-600 hover:rounded-lg">
          <FontAwesomeIcon icon={faBook} />
          <a className="px-2">Projects</a>
        </li>
        <li className="py-2 px-2 hover:bg-slate-600 hover:rounded-lg">
          <FontAwesomeIcon icon={faAddressCard} />
          <a className="px-2">Contact</a>
        </li>
      </ul>
    </div>
  );
}

export default SideBar;
